import { Controller, Get } from '@nestjs/common';

@Controller('/')
export class AppController {
  @Get()
  findAll(): string {
    return (
      'version-' +
      process.env.CI_COMMIT_REF_NAME.slice(
        process.env.CI_COMMIT_REF_NAME.indexOf('-') + 1,
      )
    );
  }
}
